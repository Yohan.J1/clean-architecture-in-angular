import { TestBed } from '@angular/core/testing';

import { ListBreedService } from './list-breed.service';

describe('ListBreedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ListBreedService = TestBed.get(ListBreedService);
    expect(service).toBeTruthy();
  });
});
