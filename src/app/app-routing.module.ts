import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BreedsComponent } from './components/breeds/breeds.component'
import { BreedsListComponent } from './components/breeds-list/breeds-list.component'


const routes: Routes = [
  {
    path: '',
    component: BreedsListComponent
  },
  {
    path: 'breed/:breed/details',
    component: BreedsComponent
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
