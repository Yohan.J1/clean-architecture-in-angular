import { Component, OnInit } from '@angular/core';
import { ListBreedService } from "../../core/usecase/list-breed.service";
import { DetailsBreedService } from "../../core/usecase/details-breed.service";

@Component({
  selector: 'app-breeds-list',
  templateUrl: './breeds-list.component.html',
  styleUrls: ['./breeds-list.component.css']
})
export class BreedsListComponent implements OnInit {

  public arrayBreeds: Array<any> = [];

  constructor(
    private breedList: ListBreedService,
    private breedsDetails: DetailsBreedService
  ) { }

  ngOnInit() { this.getAllBreeds(); }

  getAllBreeds(){
    this.breedList.getAllBreeds().subscribe((resp: any)=> {
      console.log('resp=>',resp);

      let content = Object.keys(resp.message)
      console.log('CONT=>', content)

      content.forEach(item =>{
        this.breedsDetails.getBreed(item).subscribe((resp: any) =>{
          console.log('resss=>',resp)
          this.arrayBreeds.push({ name: item, img: resp.message })
        });
      });
    })
  }
}
