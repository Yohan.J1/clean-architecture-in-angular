import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Api } from "../../data/repository/api";

@Injectable({
  providedIn: 'root'
})
export class ListBreedService {

  allBreeds = Api.Instace.url+'breeds/list/all'

  constructor( private http: HttpClient ) { }

  getAllBreeds(){
    return this.http.get(this.allBreeds);
  }
}
