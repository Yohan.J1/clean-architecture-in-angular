import { TestBed } from '@angular/core/testing';

import { DetailsBreedService } from './details-breed.service';

describe('DetailsBreedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DetailsBreedService = TestBed.get(DetailsBreedService);
    expect(service).toBeTruthy();
  });
});
