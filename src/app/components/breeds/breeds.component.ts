import { Component, OnInit } from '@angular/core';
import { DetailsBreedService } from "../../core/usecase/details-breed.service";
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-breeds',
	templateUrl: './breeds.component.html',
	styleUrls: ['./breeds.component.css']
})
export class BreedsComponent implements OnInit {

	public dataBreed: Array<any> = [];
	public nameBreed: String;

	constructor(
		private oneBreed: DetailsBreedService,
		private routeActive: ActivatedRoute
	) { this.nameBreed = this.routeActive.snapshot.params.breed }

	ngOnInit() { this.getBreed() }

	getBreed(){
		this.oneBreed.getBreed(this.nameBreed).subscribe((resp: any)=> {
			this.dataBreed = resp.message
			console.log('breed=>', this.dataBreed);
		},
		err =>{
			console.log('BreedERR=>', err)
		})
	}

}
