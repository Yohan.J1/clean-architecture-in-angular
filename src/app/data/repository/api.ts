import { Injectable } from '@angular/core';

@Injectable()

export class Api {

    private static instace: Api;
    
    public url: String;
    
    constructor(){
        this.url = 'https://dog.ceo/api/';
    }
    
    public static get Instace(){
        return this.instace || (this.instace = new this())
    }
}