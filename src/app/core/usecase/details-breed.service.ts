import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Api } from "../../data/repository/api";

@Injectable({
  providedIn: 'root'
})
export class DetailsBreedService {
  
  onlyBreed = Api.Instace.url+'breed/'

  constructor( private http: HttpClient ) { }

  getBreed(breed){
    return this.http.get(this.onlyBreed+breed);
  }
}
